# Resume
My resume project, written in LaTeX. This is the public project, which only contains example data.

# Background
Over the years I've ended up with a filesystem full of different static resume documents. Variations made for particular jobs, companies, or roles, required their own document file. Keeping track of these documents, across multiple devices, and in email, was frustrating. Keeping formatting consistent over the years, across Word document versions, has been a challenge.

# Structure
This project is the public component of my resume project. It contains the logic, structure, and example data to test the document compilation and themes. A separate project is required to host your individual data.

I wanted to keep this part public so others can view, contribute or fork, while keeping my personal data private.

# Why LaTeX?
A resume will be a document that I will use for my entire career. The lifetime of this document will span decades. I want to pick a format and workflow that can work for that entire timeframe, without much change.

Tex and LaTeX are already decades old, widely used and supported, and stable. I can design modular documents that will not need much maintenance over time.

# Why Git?
Version control in traditional word processing programs leaves a lot to be desired. Git provides excellent version control, is the industry standard, and will be around for the long haul.

Git branching is the perfect way to preserve history for different resume versions over time.

# How to use
Once you clone this project, you will need to ensure you create a new private project to contain your personal data. The private project directory should exist inside the root of the cloned project.

To populate your private project with the public `example-data` files to use as a starting point:

```
mkdir private-data

cd private-data

# Download tar of public project
wget https://gitlab.com/mickotronic/latex-resume/-/archive/main/latex-resume-main.tar -P /tmp

# Untar `example-data` to private project
tar -xvf /tmp/latex-resume-main.tar --strip-components=2 latex-resume-main/example-data

git init --initial-branch=main

git remote add origin git@gitlab.com:<namespace>/latex-resume-private.git

git add .

git commit -m "Initial commit"

git push -u origin main
```

Ensure you change the data and files to suit your resume.