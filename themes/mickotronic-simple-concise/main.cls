\ProvidesClass{main}[2022/02/01 Main class]

\LoadClass[9pt,a4paper]{extarticle}
\RequirePackage[includeheadfoot,a4paper, left=0.40in,top=0.3in,right=0.75in,bottom=0.7in]{geometry}
\setlength\parindent{0pt}
\usepackage{import}
\usepackage[utf8]{inputenc}
\RequirePackage{fontawesome}
\usepackage[dvipsnames]{xcolor}

\renewcommand{\familydefault}{\sfdefault}

\usepackage{fancyhdr}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=black,
    pdfpagemode=FullScreen
}
\urlstyle{same}

\usepackage{titlesec}